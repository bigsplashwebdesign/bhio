"use strict";

jQuery(function ($) {
  if ($('body').hasClass('ios')) {
    $('INSERT SELECTOR HERE').on('click touchend', function (e) {
      var el = $(this);
      var link = el.attr('href');
      window.location = link;
    });
  }

  $('.menu-item-has-children').each(function () {
    var link = $(this).find('> a');
    link.replaceWith(link[0].outerHTML + '<button class="menu-toggler"><svg class="inline-block mb-1" width="8" height="6" viewBox="0 0 8 6" fill="none" xmlns="http://www.w3.org/2000/svg"><path d = "M3.75 5.21875H3.71875C3.78125 5.28125 3.875 5.3125 4 5.3125C4.09375 5.3125 4.1875 5.28125 4.28125 5.21875L7.90625 1.53125C7.96875 1.46875 8 1.40625 8 1.28125C8 1.1875 7.96875 1.09375 7.90625 1L7.65625 0.78125C7.59375 0.71875 7.5 0.6875 7.40625 0.6875C7.28125 0.6875 7.1875 0.71875 7.125 0.78125L4 3.96875L0.875 0.78125C0.78125 0.71875 0.6875 0.65625 0.59375 0.65625C0.46875 0.65625 0.40625 0.71875 0.34375 0.78125L0.09375 1C0.03125 1.09375 0 1.1875 0 1.28125C0 1.40625 0.03125 1.46875 0.125 1.53125L3.75 5.21875Z" fill="white" /></svg></button>');
  });
  $('#mobile-menu').on('click', function () {
    if ($(this).hasClass('active')) {
      $('.sub-menu__wrapper').collapse('hide');
      $('.menu-toggler').removeClass('active');
      $(this).removeClass('active');
    } else {
      $(this).addClass('active');
    }

    $('#nav').toggleClass('active');
  }); // toggle dropdown menu

  $('.menu-toggler').on('click', function () {
    if (!$(this).hasClass('active')) {
      $(this).addClass('active');
      $(this).parent('li').find('.sub-menu__wrapper').collapse('show');
    } else {
      $(this).removeClass('active');
      $(this).parent('li').find('.sub-menu__wrapper').collapse('hide');
    }
  });
  $('.menu-item-has-children > a').on('click', function () {
    var arrow = $(this).parent('li').find('.menu-toggler');

    if (arrow.hasClass('active')) {
      arrow.removeClass('active');
      $(this).parent('li').find('.sub-menu__wrapper').collapse('hide');
    } else {
      $(this).parent('li').find('.sub-menu__wrapper').collapse('show');
      arrow.addClass('active');
    }
  }); // function moveTranslateDropdown() {
  // 	var dropdownGoogle = $('#google_translate_element');
  // 	var winWidth = $(window).width();
  // 	var parent;
  // 	if(winWidth < 992) {
  // 		parent = $('.google-translate-dropdown.mobile');
  // 	}
  // 	else parent = $('google-translate-dropdown.desktop');
  // 	parent.html(dropdownGoogle);
  // }
  // moveTranslateDropdown();
  // $(window).resize(moveTranslateDropdown);

  var videoSrc;
  var video_btn = $('.video-btn');

  if (video_btn.length > 0) {
    video_btn.on('click', function (e) {
      e.preventDefault();
      videoSrc = $(this).attr('data-video-src');
    }); // opened modal

    $('#video-modal').on('shown.bs.modal', function (e) {
      $('#video').attr('src', videoSrc + "?autoplay=1&showinfo=0&modestbranding=1&rel=0&mute=1");
    }); // on closed modal

    $('#video-modal').on('hide.bs.modal', function (e) {
      $('#video').attr('src', '');
    });
  } // Related articles


  var related_articles_args = {
    slidesToShow: 1,
    slidesToScroll: 1,
    mobileFirst: true,
    speed: 800,
    dots: true,
    arrows: true,
    appendDots: $('#related-article-dots'),
    prevArrow: $('.related-prev'),
    nextArrow: $('.related-next'),
    responsive: [{
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }]
  };
  var related_articles_slider = $('#related_articles');
  if (related_articles_slider.length > 0) related_articles_slider.slick(related_articles_args); // Download PDF button
  // var download_btn = $('#download_pdf_btn');
  // if(download_btn.length > 0) {
  // 	download_btn.on('click', function(e) {
  // 		e.preventDefault();
  // 	});
  // }

  $('#article-container').slick({
    rows: 3,
    // fade: true,
    infinite: false,
    dots: true,
    arrows: true,
    mobileFirst: true,
    appendDots: $('#related-article-dots'),
    prevArrow: $('.related-prev'),
    nextArrow: $('.related-next')
  });
  $('#article-container').on('afterChange', function (slick, currentSlide) {
    $('html, body').animate({
      scrollTop: $("#article-page").offset().top
    }, 1000);
  });
});