<?php
/*======================
	=Mail
========================*/
function bs_wp_mail_from( $email_address ) {
	return get_bloginfo("admin_email");
}
add_filter( 'wp_mail_from', 'bs_wp_mail_from' );

function bs_wp_mail_from_name( $email_from ) {
	return get_bloginfo("name");
}
add_filter( 'wp_mail_from_name', 'bs_wp_mail_from_name' );
?>