<?php
/*======================
	=Scripts
========================*/
function bigsplash_scripts() {
	//Styles
	wp_enqueue_style( 'bsstyle', TEMPPATH . '/style.min.css', false, '1' );
	//Scripts
	wp_enqueue_script( 'vendorscripts', TEMPPATH . '/assets/js/vendor.js', array( 'jquery' ), '1', false );
	wp_enqueue_script( 'bsscripts', TEMPPATH . '/assets/js/custom.js', array( 'jquery' ), '1', false );
}
add_action( 'wp_enqueue_scripts', 'bigsplash_scripts' );
?>
