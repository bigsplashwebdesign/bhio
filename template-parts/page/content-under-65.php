<?php $thumbnail = get_the_post_thumbnail_url(); 
	if(empty($thumbnail)) $thumbnail = get_template_directory_uri() . '/assets/images/global/banner_background.png';
?>
<article id="post-<?php the_ID(); ?>" class="container-fluid inner px-0">
	<header class="subpage-banner page-header" style="background-image: url(<?=$thumbnail?>);">
		<div class="row innerin">
			<div class="entry-header position-relative px-4 col-lg-5"><h1 class="text-white font-lexend entry-title font-36"><?php the_title(); ?></h1></div><!-- .entry-header -->
		</div>
	</header>
	<div class="entry-content row px-3 px-lg-0 innerin service-article __under-65">
		<div class="col-lg-7 py-5 article-container">
			
			<?php while(have_rows('content_blocks')) : the_row(); ?>
				<div class="mb-5 pb-5 content-block-wrapper">
					<h2 class="font-40 font-lexend font-medium mb-4"><?=get_sub_field('heading') ?></h2>
					<?=get_sub_field('content') ?>

					<?php $box = get_sub_field('bue_box'); ?>
					<?php if($box): ?>
					<div class="blue-box in-under-65">
						<h3 class="font-30 mt-0 mb-3 font-lexend font-bold"><?=$box['title']?></h3>
						<?php if($box['text']): ?>
							<?=$box['text'] ?>
						<?php endif; ?>
						
						<div><a target="_blank" href="<?=$box['button_link']['url']?>" class="mt-4 btn btn-blue on-light d-inline-block mx-auto contact-btn"><?=$box['button_text']?></a></div>

					</div>
					<?php endif; ?>
				</div>

			<?php endwhile; ?>
			
		</div>
		<div class="col-lg-5 pt-5 article-sidebar-container">
			<div class="quote-container mt-5">
				<blockquote><?=get_field("quote"); ?></blockquote>
			</div>
		</div>

		
	</div><!-- .entry-content -->

	<?php get_template_part('template-parts/components/content', 'bottom-callout-noquote'); ?>

</article><!-- #post-<?php the_ID(); ?> -->