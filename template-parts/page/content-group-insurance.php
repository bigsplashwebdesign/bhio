<?php $thumbnail = get_the_post_thumbnail_url(); 
	if(empty($thumbnail)) $thumbnail = get_template_directory_uri() . '/assets/images/global/banner_background.png';
?>
<article id="post-<?php the_ID(); ?>" class="container-fluid inner px-0">
	
	<header class="subpage-banner page-header" style="background-image: url(<?=$thumbnail?>);">
		<div class="row innerin">
			<div class="entry-header position-relative px-4 col-lg-5"><h1 class="text-white font-lexend entry-title font-36"><?php the_title(); ?></h1></div><!-- .entry-header -->
		</div>
	</header>

	<div class="entry-content">
		<?php if(have_rows('funding_advantage')):?>
		<?php while(have_rows('funding_advantage')) : the_row(); ?>
		<section class="container-fluid inner section__advantage">
			<div class="row innerin">
				<div class="col-sm-10 mx-sm-auto col-lg-6 pr-lg-5">
					<h2 class="font-40 font-medium font-lexend mb-4"><?=get_sub_field('heading') ?></h2>
					<?=get_sub_field('text'); ?>
					<?php $desc = get_sub_field('descriptions'); ?>
				</div>
			
				<div class="col-sm-10 mx-sm-auto col-lg-6">
					
					<a class="video-btn" data-video-src="<?=get_sub_field('video_url')?>" data-toggle="modal" data-target="#video-modal" href="javascript:void(0)">
					<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="#fff" class="bi bi-play-circle-fill" viewBox="0 0 16 16"><path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5z"/></svg>
					<img class="d-block mx-auto mt-5 mt-lg-0 img-fluid has-shadow" src="<?=get_sub_field('image')['url'];?>" alt="<?=get_sub_field('image')['alt'];?>"></a>
				</div>
			</div>
		</section>
		<?php endwhile; ?>
		<?php endif; ?>


		<section class="container-fluid inner section--why section--why__subpage bg-diff-blue">
			<div class="row innerin">
				<?php while(have_rows('get_started')) : the_row(); ?>
				<div class="col-md-10 mx-auto col-lg-6 mb-5 mb-lg-0">
					<a class="video-btn" data-video-src="<?=get_sub_field('video_url')?>" data-toggle="modal" data-target="#video-modal" href="#">
						<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="#fff" class="bi bi-play-circle-fill" viewBox="0 0 16 16"><path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5z"/></svg>
						<img class="has-shadow-alternate d-block img-fluid" src="<?=get_sub_field('image')['url'];?>" alt="<?=get_sub_field('image')['alt'];?>">
					</a>
				</div>

				<div class="col-md-10 mx-auto col-lg-5 offset-lg-1">
					
					<h3 class="font-lexend font-bold font-30 mb-4 text-white"><?=get_sub_field('heading'); ?></h3>
					<div class="text-white has-blue-bullets">
						<?=get_sub_field("text") ?>
					</div>
				
					<p class="mt-5"><a download href="<?=get_sub_field('button_link')['url']?>" class="btn btn-blue on-dark text-white"><?=get_sub_field('button_text') ?></a></p>
				</div>
				<?php endwhile; ?>

			</div>
		</section>

		<section class="container-fluid inner section__blocks group__insurance">
			<div class="row innerin align-items-stretch">
				<?php while(have_rows('blocks')) : the_row(); ?>
				<div class="col-lg-6 col-md-10 mx-md-auto mb-4">
					<div class="block-container h-100">
						<article class="h-100 mb-lg-0 article d-flex flex-column justify-content-between">
							<h3 class="font-lexend font-bold font-30 article__title mb-4"><?php echo get_sub_field('block_heading') ?></h3>
							<p><?php echo get_sub_field('block_text') ?></p>
							<a href="<?=get_sub_field('button_link')['url']?>" class="btn btn-blue on-light mt-3 align-self-lg-start"><?php echo get_sub_field('button_text') ?></a>
					</article>
					</div>
				</div>
				<?php endwhile; ?>
			</div>
			
		</section>

		<?php get_template_part('template-parts/components/content', 'bottom-callout-noquote'); ?>

	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->

<!-- Video Modal -->
<div class="video-modal modal fade" id="video-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<button type="button" class="close modal-dismiss" data-dismiss="modal" aria-label="Close"><span></span></button>

	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">

			<div class="modal-body p-0">
				<!-- 16:9 aspect ratio -->
				<div class="embed-responsive embed-responsive-16by9 mb-0">
					<iframe class="embed-responsive-item" src="" id="video"
						allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>

		</div>
	</div>
</div>
<!-- End video modal -->
