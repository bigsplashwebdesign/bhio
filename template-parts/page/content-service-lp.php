<?php $thumbnail = get_the_post_thumbnail_url(); 
	if(empty($thumbnail)) $thumbnail = get_template_directory_uri() . '/assets/images/global/banner_background.png';
?>
<div id="post-<?php the_ID(); ?>" class="container-fluid inner px-0">
	
	<header class="subpage-banner page-header" style="background-image: url(<?=$thumbnail?>);">
		<div class="row innerin">
			<div class="col-lg-6 entry-header position-relative px-4 "><h1 class="text-white font-lexend entry-title font-36"><?php the_title(); ?></h1></div><!-- .entry-header -->
		</div>
	</header>

	<div class="entry-content">
		
		<section class="container-fluid inner section--articles">
			<div class="row innerin">
				<?php $children = get_field('services_to_include'); ?>
				<?php if($children): ?>
				<?php foreach($children as $post): ?>
				<?php setup_postdata($post); ?>
				<?php $excerpt = get_field('excerpt'); ?>
				<div class="col-lg-6 article-container  mb-4">
					
					<article class="h-100 d-flex flex-column justify-content-between">
						<h3 class="font-lexend font-bold font-30 article__title mb-4"><?=get_the_title();?></h3>
						<p><?php if($excerpt) echo $excerpt; ?></p>
						<a href="<?=get_the_permalink();?>" class="align-self-lg-start btn btn-blue on-light mt-3">Learn more</a>
					</article>

				</div>
				<?php endforeach; ?>
				<?php endif; ?>

			</div>
		</section>

		<?php get_template_part('template-parts/components/content', 'bottom-callout'); ?>

	</div><!-- .entry-content -->
</div><!-- #post-<?php the_ID(); ?> -->