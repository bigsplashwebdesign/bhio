<?php $thumbnail = get_the_post_thumbnail_url(); 
	if(empty($thumbnail)) $thumbnail = get_template_directory_uri() . '/assets/images/global/banner_background.png';
?>
<article id="post-<?php the_ID(); ?>" class="container-fluid inner px-0">
	
	<header class="subpage-banner page-header" style="background-image: url(<?=$thumbnail?>);">
		<div class="row innerin">
			<div class="entry-header position-relative px-4"><h1 class="text-white font-lexend entry-title font-36"><?php the_title(); ?></h1></div><!-- .entry-header -->
		</div>
	</header>

	<div class="entry-content">
		<?php if(have_rows('who_we_are')):?>
		<?php while(have_rows('who_we_are')) : the_row(); ?>
		<section class="container-fluid inner section__who-we-are">
			<div class="row innerin">
				<div class="col-sm-10 mx-sm-auto col-lg-12">
					<h2 class="font-40 font-lexend mb-4">Who We Are</h2>
				</div>
			</div>

			<div class="row innerin">
				<div class="col-sm-10 mx-sm-auto col-lg-7 pr-lg-5">
					<?=get_sub_field('main_text'); ?>
					<?php $desc = get_sub_field('descriptions'); ?>
					<?php if($desc): ?>
						<?php foreach($desc as $index=>$des): ?>
						<p class="mb-2 mt-4 text-uppercase font-bold font-lexend"><?=$des['title']; ?></p>
						<p><?=$des['text']?></p>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			
				<div class="col-sm-10 mx-sm-auto col-lg-5">
					<img class="d-block mx-auto mt-5 mt-lg-0 img-fluid" src="<?=get_sub_field('image')['url'];?>" alt="<?=get_sub_field('image')['alt'];?>">
				</div>
			</div>
		</section>
		<?php endwhile; ?>
		<?php endif; ?>


		<section class="container-fluid inner section--why section--why__subpage bg-diff-blue">
			<div class="row innerin align-items-center">
				<div class="col-sm-10 col-lg-9 mx-auto col-xl-6 offset-xl-1 mt-lg-5 mt-xl-0">
					<h2 class="font-lexend font-40 mb-4 text-white">Why Nassau Bay Agency?</h2>
					<p class="text-white">Where you choose to purchase your insurance protection makes a difference. Two key reasons to choose us over others are:</p>
					<div class="check my-5 d-md-flex">
						<div class="mb-5 mb-lg-0 check__content left text-center">
							<svg width="35" height="35" viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="17.5" cy="17.5" r="17.5" fill="#33A6DE"/><path d="M25.1912 10.6555C25.2766 10.5701 25.3621 10.5273 25.533 10.5273C25.6611 10.5273 25.7893 10.5701 25.9175 10.6555L27.1138 11.8945C27.1992 11.98 27.2847 12.1082 27.2847 12.2363C27.2847 12.4072 27.1992 12.5354 27.1138 12.6208L14.2964 25.4382C14.2109 25.5237 14.0828 25.5664 13.9546 25.5664C13.7837 25.5664 13.6555 25.5237 13.5701 25.4382L7.8877 19.7131C7.75952 19.6277 7.7168 19.5422 7.7168 19.3713C7.7168 19.2432 7.75952 19.115 7.8877 18.9868L9.08398 17.7905C9.16943 17.7051 9.29761 17.6196 9.46851 17.6196C9.59668 17.6196 9.72485 17.7051 9.8103 17.7905L13.9546 21.9348L25.1912 10.6555Z" fill="white"/></svg>
							<span class="color-light-blue text-center">Our commitment to customer service</span>
						</div>
						<div class="check__content right text-center">
							<svg width="35" height="35" viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="17.5" cy="17.5" r="17.5" fill="#33A6DE"/><path d="M25.1912 10.6555C25.2766 10.5701 25.3621 10.5273 25.533 10.5273C25.6611 10.5273 25.7893 10.5701 25.9175 10.6555L27.1138 11.8945C27.1992 11.98 27.2847 12.1082 27.2847 12.2363C27.2847 12.4072 27.1992 12.5354 27.1138 12.6208L14.2964 25.4382C14.2109 25.5237 14.0828 25.5664 13.9546 25.5664C13.7837 25.5664 13.6555 25.5237 13.5701 25.4382L7.8877 19.7131C7.75952 19.6277 7.7168 19.5422 7.7168 19.3713C7.7168 19.2432 7.75952 19.115 7.8877 18.9868L9.08398 17.7905C9.16943 17.7051 9.29761 17.6196 9.46851 17.6196C9.59668 17.6196 9.72485 17.7051 9.8103 17.7905L13.9546 21.9348L25.1912 10.6555Z" fill="white"/></svg>
							<span class="color-light-blue text-center">Our approach of helping you fit insurance coverage to your needs and not just buy a policy.</span>
						</div>
					</div>
					<p class="text-white">We’re proud to serve families and businesses of all sizes for multiple generations. We look forward to the possibility of serving you.</p>
					
				</div>

				<div class="col-sm-10 col-lg-9 col-xl-5 offset-lg-1 mx-auto mission-value-container">
				<?php while(have_rows('mission_and_values')) : the_row(); ?>
					<div class="mission-value mission">
					<p class="text-white font-lexend font-bold text-uppercase"><?=get_sub_field('mission__title')?></p>
					<p class="mb-5 text-white"><?=get_sub_field('mission_text')?></p>
					</div>

					<div class="mission-value value">
					<p class="text-white font-lexend font-bold text-uppercase"><?=get_sub_field('value_title')?></p>
					<div class="values text-white"><?=get_sub_field('values'); ?></div>
					</div>
				<?php endwhile; ?>
				</div>

			</div>
		</section>

		<section class="container-fluid inner section__licensed">
			<?php while(have_rows('license')) : the_row(); ?>
			<div class="row innerin">
				<div class="col">
					<h2 class="font-40 font-lexend text-center"><?=get_sub_field('heading')?></h2>
				</div>
			</div>
			<div class="row innerin">
				<div class="col-md-10 col-lg-6 mx-auto">
					<div class="state-container">
						<?php $states = get_sub_field('states'); ?>
						<ul class="state-list blue-bulleted-list">
						<?php foreach($states as $state): ?>
							<li><?=$state['state']?></li>
						<?php endforeach; ?>
						</ul>
					</div>
				</div>
			</div>
			<?php endwhile; ?>
		</section>

		<?php get_template_part('template-parts/components/content', 'bottom-callout'); ?>

	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->