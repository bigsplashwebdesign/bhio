<?php $thumbnail = get_the_post_thumbnail_url(); 
	if(empty($thumbnail)) $thumbnail = get_template_directory_uri() . '/assets/images/global/banner_background.png';
?>
<article id="post-<?php the_ID(); ?>" class="container-fluid inner px-0">
	
	<header class="subpage-banner page-header" style="background-image: url(<?=$thumbnail?>);">
		<div class="row innerin">
			<div class="entry-header position-relative px-4 col-lg-5"><h1 class="text-white font-lexend entry-title font-36"><?php the_title(); ?></h1></div><!-- .entry-header -->
		</div>
	</header>

	<div class="entry-content">
		
		<section class="container-fluid inner service-subpage__content section__subpage pb-0">
			<div class="row innerin">
				<div class="col-sm-10 col-lg-7">
					<?php the_content(); ?>
				</div>

				<div class="col-lg-5 pt-5 article-sidebar-container">
					<div class="quote-container mt-5">
						<blockquote><?=get_field("quote"); ?></blockquote>
					</div>
				</div>
			</div>
		</section>

		<!-- <section class="container-fluid inner section__accordion">
			<div class="row innerin mt-5">
				<div class="col-lg-9 accordion-container">
					<div class="accordion" id="benefit-accordion">
						<?php while(have_rows('benefits')) : the_row(); ?>
						<?php $index = get_row_index(); ?>
						<div class="card">
							<div class="card-header" id="heading<?=$index;?>">
								
									<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?=$index;?>" aria-expanded="<?=$index == 1 ? 'true' : 'false'?>" aria-controls="collapse<?=$index;?>">
										<?=get_sub_field('benefit_title'); ?>
									</button>

									<button class="accordion-arrow" data-toggle="collapse" data-target="#collapse<?=$index;?>" aria-controls="collapse<?=$index;?>" aria-expanded="<?=$index == 1 ? 'true' : 'false'?>">
									<svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.81641 1.01562C6.71094 0.945312 6.60547 0.910156 6.5 0.910156C6.35938 0.910156 6.28906 0.945312 6.21875 1.01562L0.980469 6.18359C0.910156 6.28906 0.875 6.39453 0.875 6.5C0.875 6.60547 0.910156 6.71094 1.01562 6.78125L1.68359 7.48438C1.75391 7.55469 1.85938 7.58984 2 7.58984C2.10547 7.58984 2.21094 7.55469 2.28125 7.48438L6.5 3.30078L10.7188 7.48438C10.7891 7.55469 10.8594 7.58984 11 7.58984C11.1055 7.58984 11.2109 7.55469 11.3164 7.48438L12.0195 6.78125C12.0898 6.71094 12.125 6.60547 12.125 6.5C12.125 6.39453 12.0898 6.28906 12.0195 6.18359L6.81641 1.01562Z" fill="white"/></svg>
									</button>
								
							</div>

							<div id="collapse<?=$index;?>" class="collapse <?= $index == 1 ? 'show' : '';?>" aria-labelledby="heading<?=$index;?>">
								<div class="card-body">
									<?php $contents = get_sub_field('benefit_content'); ?>
									<div class="px-0 card-body-wrapper container-fluid">
										<div class="row">
											<?php foreach($contents as $content): ?>
											<div class="mb-4 mb-lg-0 col-lg-6 pr-lg-5">
												<h4><?php echo $content['benefit_sub_heading'] ?></h4>
												<?php echo $content['benefit_content_block'] ?>
											</div>
											<?php endforeach; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php endwhile; // end while('benefits') ?>
						
					</div>
				</div>
			</div>
		</section> -->

		<section class="section__tabs container-fluid inner">
			<div class="row innerin">
				<div class="col tabs">
					<?php $tabs = get_field('benefits'); ?>
					<nav class="tabs__conditions">
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<?php $index = 0; ?>
							<?php foreach($tabs as $tab): ?>
							<a class="nav-item nav-link <?=$index++ == 0 ? 'active' : '' ?>" id="nav-<?=$index?>-tab" data-toggle="tab" href="#nav-<?=$index?>" role="tab" aria-controls="nav-<?=$index?>" aria-selected="<?=$index == 0 ? 'true' : 'false' ?>"><h3 class="mb-0 font-medium font-18"><?=$tab['benefit_title'] ?></h3></a>
							<?php endforeach; ?>							
						</div>

					</nav>
					<div class="tab-content" id="nav-tabContent">
						<?php $index = 0; ?>
						<?php foreach($tabs as $tab): ?>
						<?php $content = $tab['benefit_content']; ?>
						<div class=" p-3 p-lg-5 tab-pane fade <?=$index++ == 0 ? 'active show' : '' ?>" id="nav-<?=$index?>" role="tabpanel" aria-labelledby="nav-<?=$index?>-tab">
							<div class="container-fluid px-0">
								<div class="row">
									<?php foreach($content as $cont): ?>
									<div class="mb-4 mb-lg-0 col-lg-6 pr-lg-5 tab-content">
										<h4 class="font-24 font-bold"><?= $cont['benefit_sub_heading']; ?></h4>
										<?= $cont['benefit_content_block']; ?>
										<!-- <h4><?= $content['benefit_sub_heading']; ?></h4>
										<?= $content['benefit_content_block']; ?> -->
									</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</section>


		<section class="container-fluid inner section__footnote">
			<div class="row innerin">
				<div class="col-lg-7 py-5">
					<?php echo get_field('footer_text'); ?>
				</div>
			</div>
		</section>

		<?php get_template_part('template-parts/components/content', 'bottom-callout-noquote'); ?>

	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->