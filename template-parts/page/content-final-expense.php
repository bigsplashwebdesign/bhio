<?php $thumbnail = get_the_post_thumbnail_url(); 
	if(empty($thumbnail)) $thumbnail = get_template_directory_uri() . '/assets/images/global/banner_background.png';
?>
<div id="post-<?php the_ID(); ?>" class="container-fluid inner px-0">
	
	<header class="subpage-banner page-header" style="background-image: url(<?=$thumbnail?>);">
		<div class="row innerin">
			<div class="col-lg-6 entry-header position-relative px-4"><h1 class="text-white font-lexend entry-title font-36 font-medium"><?php the_title(); ?></h1></div><!-- .entry-header -->
		</div>
	</header>

	<div class="entry-content page__life-insurance">
		
		<section class="container-fluid inner">
			<div class="row innerin">
				<div class="col-lg-7 __final-expense service-article mt-5 mt-lg-0">
				 <?php the_content(); ?>
				</div>	
			</div>
		</section>

		<section class="container-fluid inner section__blocks py-0 mt-5">
			<div class="row innerin align-items-stretch">
				<?php while(have_rows('blocks')) : the_row(); ?>
				<div class="col-lg-6 col-md-10 mx-md-auto mb-4">
					<div class="block-container h-100">
						<article class="h-100 mb-lg-0 article d-flex flex-column justify-content-between">
							<h3 class="font-lexend font-bold font-30 article__title mb-4"><?php echo get_sub_field('block_heading') ?></h3>
							<p><?php echo get_sub_field('block_text') ?></p>
							<a href="<?=get_sub_field('button_link')['url']?>" class="btn btn-blue on-light mt-3 align-self-start"><?php echo get_sub_field('button_text') ?></a>
					</article>
					</div>
				</div>
				<?php endwhile; ?>
			</div>
		</section>

		<?php if(have_rows('tab_section')): ?>
		<?php while(have_rows('tab_section')) : the_row(); ?>
		<section class="section__tabs final-expense mt-5 container-fluid inner">
			<div class="row innerin">
				<div class="col">
					<h2 class="font-lexend font-40 text-center font-medium"><?=get_sub_field('tab_heading')?></h2>
				</div>
			</div>
			<div class="row innerin">
				<div class="col tabs mt-5">
					<?php $tabs = get_sub_field('tabs'); ?>
					<nav class="tabs__conditions">
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<?php $index = 0; ?>
							<?php foreach($tabs as $tab): ?>
							<a class="nav-item nav-link <?=$index++ == 0 ? 'active' : '' ?>" id="nav-<?=$index?>-tab" data-toggle="tab" href="#nav-<?=$index?>" role="tab" aria-controls="nav-<?=$index?>" aria-selected="<?=$index == 0 ? 'true' : 'false' ?>"><?=$tab['title'] ?></a>
							<?php endforeach; ?>							
						</div>

					</nav>
					<div class="tab-content" id="nav-tabContent">
						<?php $index = 0; ?>
						<?php foreach($tabs as $tab): ?>
						<?php $content = $tab['tab_content']; ?>
						<div class="tab-pane fade <?=$index++ == 0 ? 'active show' : '' ?>" id="nav-<?=$index?>" role="tabpanel" aria-labelledby="nav-<?=$index?>-tab">
							<div class="container-fluid px-0">
								<div class="row">
									<div class="col-lg-7 p-5 tab-content">
										<?php echo $content; ?>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</section>
		<?php endwhile; ?>
		<?php endif; ?>


		<?php get_template_part('template-parts/components/content', 'bottom-callout'); ?>

	</div><!-- .entry-content -->
</div><!-- #post-<?php the_ID(); ?> -->