<?php $thumbnail = get_the_post_thumbnail_url(); 
	if(empty($thumbnail)) $thumbnail = get_template_directory_uri() . '/assets/images/global/banner_background.png';
?>
<div id="post-<?php the_ID(); ?>" class="container-fluid inner px-0">
	
	<header class="subpage-banner page-header" style="background-image: url(<?=$thumbnail?>);">
		<div class="row innerin">
			<div class="entry-header position-relative px-4"><h1 class="text-white font-lexend entry-title font-36"><?php the_title(); ?></h1></div><!-- .entry-header -->
		</div>
	</header>

	<div class="entry-content">
		
		<section class="container-fluid inner section--articles" id="article-page">
			<div class="row innerin">
				<div class="col-lg-6 article-container">
					<div id="article-container" class="overflow-hidden">
					<?php 
						$args = array(
							'post_type' => 'article',
							'posts_per_page' => -1
						); 
						$articles = new WP_Query($args);
						while($articles->have_posts()) : $articles->the_post();
					?>
					<article class="helpdesk-article">
						<h3 class="font-lexend font-bold font-30 article__title mb-4"><a class="article__title--link" href="<?=get_the_permalink();?>"><?=get_the_title();?></a></h3>
						<p><?php if(has_excerpt()) echo get_the_excerpt(); 
						else echo wp_trim_words(get_the_content(), 30); ?></p>
						<a href="<?=get_the_permalink();?>" class="btn btn-blue on-light mt-3">Read more</a>
					</article>
					<?php endwhile; ?>
					</div>

					<div class="slider-component-container">
						<button class="slick-arrow related-prev slick-prev"><svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.1875 0.0950226C7.125 0.0316742 7.03125 0 6.90625 0C6.8125 0 6.71875 0.0316742 6.65625 0.0950226L0.125 6.74661C0.0625 6.80996 0 6.90498 0 7C0 7.1267 0.0625 7.19005 0.125 7.25339L6.65625 13.905C6.71875 13.9683 6.8125 14 6.90625 14C7.03125 14 7.125 13.9683 7.1875 13.905L7.8125 13.2715C7.875 13.2081 7.90625 13.1448 7.90625 13.0181C7.90625 12.9231 7.875 12.8281 7.8125 12.733L2.96875 7.82353H13.625C13.75 7.82353 13.8438 7.79186 13.9062 7.72851C13.9688 7.66516 14 7.57014 14 7.44344V6.55656C14 6.46154 13.9688 6.36652 13.9062 6.30317C13.8438 6.23982 13.75 6.17647 13.625 6.17647H2.96875L7.8125 1.26697C7.875 1.20362 7.90625 1.1086 7.90625 0.981901C7.90625 0.886878 7.875 0.791855 7.8125 0.728507L7.1875 0.0950226Z" fill="#454D58"/></svg></button>
						<div class="dots-container" id="related-article-dots"></div>
						<button class="slick-arrow related-next slick-next"><svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.8125 0.0950226C6.875 0.0316742 6.96875 0 7.09375 0C7.1875 0 7.28125 0.0316742 7.34375 0.0950226L13.875 6.74661C13.9375 6.80996 14 6.90498 14 7C14 7.1267 13.9375 7.19005 13.875 7.25339L7.34375 13.905C7.28125 13.9683 7.1875 14 7.09375 14C6.96875 14 6.875 13.9683 6.8125 13.905L6.1875 13.2715C6.125 13.2081 6.09375 13.1448 6.09375 13.0181C6.09375 12.9231 6.125 12.8281 6.1875 12.733L11.0312 7.82353H0.375C0.25 7.82353 0.15625 7.79186 0.09375 7.72851C0.03125 7.66516 0 7.57014 0 7.44344V6.55656C0 6.46154 0.03125 6.36652 0.09375 6.30317C0.15625 6.23982 0.25 6.17647 0.375 6.17647H11.0312L6.1875 1.26697C6.125 1.20362 6.09375 1.1086 6.09375 0.981901C6.09375 0.886878 6.125 0.791855 6.1875 0.728507L6.8125 0.0950226Z" fill="#454D58"/></svg></button>
					</div>
					<!-- End slider navigation -->
				</div>

				<div class="col-lg-5 offset-lg-1 article-sidebar-container">
					<?php get_template_part('template-parts/components/content', 'sidebar-video'); ?>
				</div>

			</div>
		</section>

		<?php get_template_part('template-parts/components/content', 'bottom-callout'); ?>

	</div><!-- .entry-content -->
</div><!-- #post-<?php the_ID(); ?> -->