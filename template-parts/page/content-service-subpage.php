<?php $thumbnail = get_the_post_thumbnail_url(); 
	if(empty($thumbnail)) $thumbnail = get_template_directory_uri() . '/assets/images/global/banner_background.png';
?>
<article id="post-<?php the_ID(); ?>" class="container-fluid inner px-0">
	<header class="subpage-banner page-header" style="background-image: url(<?=$thumbnail?>);">
		<div class="row innerin">
			<div class="entry-header position-relative px-4 col-lg-6"><h1 class="text-white font-lexend entry-title font-36"><?php the_title(); ?></h1></div><!-- .entry-header -->
		</div>
	</header>
	<div class="entry-content row innerin service-article __msa">
		<div class="col-lg-10 col-xl-8 pr-lg-5 py-5 article-container">
			
			<?php the_content(); ?>

			<?php if(is_page(38) || is_page(40) ): ?>
			<div class="button-continer <?=is_page(38) ? 'my-5' : 'mt-5' ?>">
				<?php while(have_rows('buttons')):the_row(); ?>
				<a target="<?=get_sub_field('button_link')['target']?>" href="<?=get_sub_field('button_link')['url']?>" class="btn btn-blue on-light d-inline-block mb-3"><?=get_sub_field('button_text')?></a>
				<?php endwhile; ?>
			</div>
			<?php endif; ?>

			<?php if(is_page(40)): ?>
				<div class="container-fluid px-0 mt-5">
					<div class="row innerin">
						<div class="col-lg-9 mb-5 px-0">
							<div class="blue-box">
								<?php while(have_rows('block', 40)) : the_row(); ?>
								<h3 class="blue-box__title font-30 font-bold font-lexend mb-4"><?=get_sub_field("title")?></h3>
								<p><?=get_sub_field('text')?></p>
								<a href="<?=get_sub_field('button_link')['url']?>" class="btn btn-blue on-light d-inline-block"><?=get_sub_field('button_text')?></a>
								<?php endwhile; ?>
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>
				
		</div>
	</div><!-- .entry-content -->

	<?php get_template_part('template-parts/components/content', 'bottom-callout'); ?>

</article><!-- #post-<?php the_ID(); ?> -->