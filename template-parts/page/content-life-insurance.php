<?php $thumbnail = get_the_post_thumbnail_url(); 
	if(empty($thumbnail)) $thumbnail = get_template_directory_uri() . '/assets/images/global/banner_background.png';
?>
<div id="post-<?php the_ID(); ?>" class="container-fluid inner px-0">
	
	<header class="subpage-banner page-header" style="background-image: url(<?=$thumbnail?>);">
		<div class="row innerin">
			<div class="col-lg-6 entry-header position-relative px-4"><h1 class="text-white font-lexend entry-title font-36"><?php the_title(); ?></h1></div><!-- .entry-header -->
		</div>
	</header>

	<div class="entry-content page__life-insurance">
		
		<section class="container-fluid inner">
			<div class="row innerin">
				<div class="col-lg-7 service-article __life-insurance mt-5 mt-lg-0">
				 <?php the_content(); ?>

				 <div class="blue-box">
					
					<h3 class="blue-box__title font-30 font-bold font-lexend mb-4"><?=get_field("box_heading")?></h3>
					<p><?=get_field('box_text')?></p>
					<a href="<?=get_field('button_link')['url']?>" class="btn btn-blue on-light d-inline-block"><?=get_field('button_text')?></a>
					
					</div>

				</div>

				<div class="col-lg-5 pt-5 article-sidebar-container">
					<div class="quote-container mt-5">
						<blockquote><?=get_field("quote"); ?></blockquote>
					</div>
				</div>

			</div>
		</section>

		<section class="section__tabs container-fluid inner">
			<div class="row innerin">
				<div class="col tabs">
					<?php $tabs = get_field('tabs'); ?>
					<nav class="tabs__conditions">
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<?php $index = 0; ?>
							<?php foreach($tabs as $tab): ?>
							<a class="nav-item nav-link <?=$index++ == 0 ? 'active' : '' ?>" id="nav-<?=$index?>-tab" data-toggle="tab" href="#nav-<?=$index?>" role="tab" aria-controls="nav-<?=$index?>" aria-selected="<?=$index == 0 ? 'true' : 'false' ?>"><?=$tab['title'] ?></a>
							<?php endforeach; ?>							
						</div>

					</nav>
					<div class="tab-content" id="nav-tabContent">
						<?php $index = 0; ?>
						<?php foreach($tabs as $tab): ?>
						<?php $content = $tab['tab_content']; ?>
						<div class="tab-pane fade <?=$index++ == 0 ? 'active show' : '' ?>" id="nav-<?=$index?>" role="tabpanel" aria-labelledby="nav-<?=$index?>-tab">
							<div class="container-fluid px-0">
								<div class="row">
									<div class="col-lg-7 p-5 tab-content">
										<?php echo $content; ?>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</section>

		<?php get_template_part('template-parts/components/content', 'bottom-callout-noquote'); ?>

	</div><!-- .entry-content -->
</div><!-- #post-<?php the_ID(); ?> -->