<?php $thumbnail = get_the_post_thumbnail_url(); 
	if(empty($thumbnail)) $thumbnail = get_template_directory_uri() . '/assets/images/global/banner_background.png';
?>
<article id="post-<?php the_ID(); ?>" class="container-fluid inner px-0">
	<header class="subpage-banner page-header" style="background-image: url(<?=$thumbnail?>);">
		<div class="row innerin">
			<div class="entry-header position-relative px-4 col-lg-8"><h1 class="text-white font-lexend entry-title font-36"><?php the_title(); ?></h1></div><!-- .entry-header -->
		</div>
	</header>
	<div class="entry-content row innerin __annuity service-article">
		<div class="col-lg-7 py-5 article-container">
			<?php the_content(); ?>

			<!-- BLue box -->
			<div class="blue-box">
				<h3 class="blue-box__title font-30 font-bold font-lexend mb-4"><?=get_field("box_heading")?></h3>
				<p><?=get_field('box_text')?></p>
				<a data-toggle="modal" data-target="#download_modal" href="#" class="btn btn-blue on-light d-inline-block" id="download_pdf_btn"><?=get_field('button_text')?></a>
			</div>
			<!-- Blue box -->
		</div>

		<div class="col-lg-5 pt-5 article-sidebar-container">
			<div class="quote-container mt-5">
				<blockquote><?=get_field("quote"); ?></blockquote>
			</div>
		</div>

	</div><!-- .entry-content -->

	<section class="container-fluid inner section--why section--why__subpage bg-diff-blue">
		<div class="row innerin">
			<?php while(have_rows('before_you_decide')) : the_row(); ?>
			<div class="col-md-10 mx-auto col-lg-6 mb-5 mb-lg-0">
				<!-- The video -->
				<div class="video-container has-shadow mb-5">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="<?=get_sub_field('video_url')?>" frameborder="0" allow="fullscreen; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
				<!-- end video -->
			</div>

			<div class="col-md-10 mx-auto col-lg-5 offset-lg-1">
				
				<h3 class="font-lexend font-bold font-30 mb-4 text-white"><?=get_sub_field('heading'); ?></h3>
				<div class="text-white has-blue-bullets">
					<?=get_sub_field("text") ?>
				</div>
				
			</div>
			<?php endwhile; ?>

		</div>
	</section>

	<?php get_template_part('template-parts/components/content', 'bottom-callout-noquote'); ?>

</article><!-- #post-<?php the_ID(); ?> -->


<!-- Download Modal -->
<div class="modal fade download-modal" id="download_modal" tabindex="-1" role="dialog" aria-labelledby="download_modal" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <h5 class="font-lexend font-24 modal-title" id="downloadmodal-title">Sign up for our FREE eBook</h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11.25 9.75L19.2773 17.7773C19.3945 17.9531 19.4531 18.1289 19.4531 18.3047C19.4531 18.5391 19.3945 18.6562 19.2773 18.7734L18.75 19.3008C18.6328 19.418 18.457 19.4766 18.2812 19.4766C18.0469 19.4766 17.8711 19.418 17.7539 19.3008L9.72656 11.2148L1.69922 19.3008C1.52344 19.418 1.34766 19.4766 1.17188 19.4766C0.9375 19.4766 0.820312 19.418 0.703125 19.3008L0.175781 18.7734C0.0585938 18.6562 0 18.5391 0 18.3047C0 18.1289 0.0585938 17.9531 0.175781 17.7773L8.26172 9.75L0.175781 1.72266C0.0585938 1.60547 0 1.42969 0 1.19531C0 1.01953 0.0585938 0.84375 0.175781 0.726562L0.703125 0.199219C0.820312 0.0820312 0.9375 0.0234375 1.17188 0.0234375C1.34766 0.0234375 1.52344 0.0820312 1.69922 0.199219L9.72656 8.22656L17.7539 0.199219C17.8711 0.0820312 18.0469 0.0234375 18.2812 0.0234375C18.457 0.0234375 18.6328 0.0820312 18.75 0.199219L19.2773 0.726562C19.3945 0.84375 19.4531 1.01953 19.4531 1.19531C19.4531 1.42969 19.3945 1.60547 19.2773 1.72266L11.25 9.75Z" fill="#454D58"/></svg>
        </button>
      </div>
      <div class="modal-body">
        <div class="download-ebook-form">
					<?php echo do_shortcode('[gravityform id="2" title="true" description="true" ajax="true"]'); ?>
				</div>
      </div>
    
    </div>
  </div>
</div>