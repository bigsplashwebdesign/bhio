<?php $thumbnail = get_the_post_thumbnail_url(); 
	if(empty($thumbnail)) $thumbnail = get_template_directory_uri() . '/assets/images/global/banner_background.png';
?>
<article id="post-<?php the_ID(); ?>" class="container-fluid inner px-0">
	
	<header class="subpage-banner page-header" style="background-image: url(<?=$thumbnail?>);">
		<div class="row innerin">
			<div class="entry-header position-relative px-4"><h1 class="text-white font-lexend entry-title font-36"><?php the_title(); ?></h1></div><!-- .entry-header -->
		</div>
	</header>

	<div class="entry-content">
		<section class="container-fluid inner section--contact bg-diff-blue">
			<div class="row innerin">
				<?php while(have_rows('contact_section')) : the_row(); ?>
				<div class="col-xl-5 col-md-8 col-lg-5 mx-lg-0 mx-md-auto pr-lg-4 pt-5">
					<h3 class="font-lexend font-40 mb-4 font-normal text-white"><?=get_sub_field('contact_heading') ?></h3>
					<p class="text-white"><?=get_sub_field('contact_text'); ?></p>
					
					<h3 class="mt-5 text-white font-30 font-bold font-lexend"><?=get_sub_field('sub_heading');?></h3>
					<ul class="contact-list mb-5">
						<?php if(get_sub_field('phone')): ?>
						<li>
							<span class="contact-icon">
								<svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M13.4805 0.666924C13.6446 0.694266 13.7539 0.776291 13.8633 0.885657C13.9453 1.02236 14 1.15907 14 1.29578C14 3.59247 13.4258 5.72512 12.2775 7.69371C11.1291 9.60762 9.62535 11.1388 7.71144 12.2598C5.74284 13.4081 3.6102 13.9823 1.3135 13.9823C1.14945 13.9823 1.01275 13.9549 0.90338 13.8456C0.766672 13.7635 0.684647 13.6268 0.684647 13.4628L0.0284494 10.6193C-0.0262337 10.4826 0.00110783 10.3458 0.0831326 10.1818C0.137816 10.0451 0.247182 9.93572 0.411232 9.88104L3.47349 8.56864C3.58286 8.51396 3.71956 8.51396 3.85627 8.5413C3.99298 8.59598 4.12969 8.65067 4.23905 8.76003L5.57879 10.4005C6.64511 9.90838 7.60207 9.25218 8.42232 8.40459C9.24256 7.58435 9.9261 6.62739 10.4183 5.56107L8.77776 4.22133C8.66839 4.11196 8.58637 4.0026 8.55902 3.86589C8.50434 3.72918 8.53168 3.59247 8.58637 3.45577L9.89876 0.393508C9.95345 0.256801 10.0355 0.147434 10.1995 0.0654094C10.3362 0.0107262 10.4729 -0.0166153 10.637 0.0107262L13.4805 0.666924Z" fill="white"/></svg>
							</span>
							<span class="contact-info phone">
								<?=get_sub_field('phone');?>
							</span>
						</li>
						<?php endif; ?>

						<?php if(get_sub_field('address')): ?>
						<li>
							<span class="contact-icon">
								<svg width="11" height="14" viewBox="0 0 11 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M4.70312 13.7266C4.8125 13.918 5.00391 14 5.25 14C5.46875 14 5.66016 13.918 5.79688 13.7266L7.62891 11.1016C8.53125 9.78906 9.13281 8.91406 9.43359 8.44922C9.84375 7.79297 10.1172 7.24609 10.2812 6.80859C10.418 6.37109 10.5 5.85156 10.5 5.25C10.5 4.32031 10.2539 3.44531 9.78906 2.625C9.29688 1.83203 8.66797 1.20312 7.875 0.710938C7.05469 0.246094 6.17969 0 5.25 0C4.29297 0 3.41797 0.246094 2.625 0.710938C1.80469 1.20312 1.17578 1.83203 0.710938 2.625C0.21875 3.44531 0 4.32031 0 5.25C0 5.85156 0.0546875 6.37109 0.21875 6.80859C0.355469 7.24609 0.628906 7.79297 1.06641 8.44922C1.33984 8.91406 1.94141 9.78906 2.87109 11.1016C3.60938 12.168 4.21094 13.043 4.70312 13.7266ZM5.25 7.4375C4.64844 7.4375 4.12891 7.24609 3.69141 6.80859C3.25391 6.37109 3.0625 5.85156 3.0625 5.25C3.0625 4.64844 3.25391 4.15625 3.69141 3.71875C4.12891 3.28125 4.64844 3.0625 5.25 3.0625C5.85156 3.0625 6.34375 3.28125 6.78125 3.71875C7.21875 4.15625 7.4375 4.64844 7.4375 5.25C7.4375 5.85156 7.21875 6.37109 6.78125 6.80859C6.34375 7.24609 5.85156 7.4375 5.25 7.4375Z" fill="white"/></svg>
							</span>
							<span class="contact-info address">
								<?=get_sub_field('address');?>
							</span>
						</li>
						<?php endif; ?>

						<?php if(get_sub_field('button_link')): ?>
						<li>
							<span class="contact-icon"></span>
							<span class="contact-info direction">
								<a target="_blank" href="<?=get_sub_field('button_link')['url']?>" class="direction__link"><?=get_sub_field('button_link')['title']?></a>
							</span>
						</li>
						<?php endif; ?>
					</ul>
				
				</div>

				<?php endwhile; ?>

				<div class="col-md-8 mx-md-auto col-lg-6 offset-lg-1">
				<div class="contact-form-wrapper bg-white">
					<?php the_content(); //echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
				</div>
				</div>

			</div>
		</section>

		<section class="container-fluid inner section--appointment" id="section-appointment">
			<?php while(have_rows('appointment_section')) : the_row(); ?>
			<div class="row innerin">
				<div class="col-lg-6 text-center mx-auto">
					<h2 class="font-40 mb-4 font-lexend text-center"><?=get_sub_field('heading')?></h2>
					<p class="text-center"><?=get_sub_field('text')?></p>
				</div>
			</div>
			<div class="row innerin mt-5 justify-content-center align-items-stretch">
				<?php $blocks = get_sub_field('blocks'); ?>
				<?php if($blocks): ?>
				<?php foreach($blocks as $index=>$block): ?>
				<?php $btn = $block['block_link']; ?>
				<div class="col-md-8 mx-md-auto mx-lg-0 col-lg-5 mb-4">
					<div class="appointment-block h-100">
						<div class="d-flex flex-column justify-content-center h-100">
							<p class="mb-4"><?=$block['block_text']?></p>
							<p class="mb-0 text-center text-lg-left"><a href="<?=$btn['url']?>" class="btn btn-blue on-light"><?=$btn['title']?></a></p>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
				<?php endif; ?>
			</div>
			<?php endwhile; ?>
		</section>

	

	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->