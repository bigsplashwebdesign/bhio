<!-- Section: Bottom callout -->
<section class="container-fluild inner section--callout">
	<div class="row inner">
		<div class="col-12 col-lg-8 mx-auto text-center">
			<?php if(have_rows('bottom_callout')): ?>
			<?php while(have_rows('bottom_callout')) : the_row(); ?>
			<h2 class="font-normal font-lexend font-bold font-30"><?=get_sub_field('title') ?></h2>
			<a href="<?=get_sub_field('button_link')['url']?>" class="my-4 btn btn-blue on-light d-inline-block mx-auto contact-btn"><?=get_sub_field('button_text')?></a>
			<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</section>