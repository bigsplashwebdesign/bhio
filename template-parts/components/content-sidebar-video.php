<div class="sidebar">
	<h3 class="sidebar-heading font-lexend font-bold font-16 text-uppercase">Video Library</h3>
	<?php 
	$args_video = array(
		'post_type' => 'video',
	); 
	$vids = new WP_Query($args_video);
	?>
	<ul class="video-list">
		<?php while($vids->have_posts()) : $vids->the_post(); ?>
		<li><a href="<?=get_the_permalink()?>"><?=get_the_title()?></a></li>
		<?php endwhile; ?>
	</ul>
</div>