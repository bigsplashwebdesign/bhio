<!-- Section: Bottom callout -->
<?php while(have_rows('')) ?>
<section class="container-fluild inner section--callout">
	<div class="row inner">
		<div class="col-md-8 col-lg-7 mx-auto text-center">
			<h2 class="font-lexend font-bold font-30">We're here to help.</h2>
			<p>Contact us for more information on our insurance products. Or shop our products directly and request a convenient self-service quote here.</p>
			<div class="button-container d-md-flex justify-content-center">
			<a href="<?=get_permalink(10)?>" class="mx-2 my-2 my-md-4 btn btn-blue on-light d-block d-sm-inline-block contact-btn">Contact us</a>
			<a target="_blank" href="<?=get_field('insuremenow_link', 'option')?>" class="mx-2 my-2 my-md-4 btn btn-blue on-light hollow-bg d-block d-sm-inline-block ">Request a quote</a>
			</div>

			<div class="phones font-lexend mt-3 mb-5 mb-lg-0">
				<svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M13.4805 0.666924C13.6446 0.694266 13.7539 0.776291 13.8633 0.885657C13.9453 1.02236 14 1.15907 14 1.29578C14 3.59247 13.4258 5.72512 12.2775 7.69371C11.1291 9.60762 9.62535 11.1388 7.71144 12.2598C5.74284 13.4081 3.6102 13.9823 1.3135 13.9823C1.14945 13.9823 1.01275 13.9549 0.90338 13.8456C0.766672 13.7635 0.684647 13.6268 0.684647 13.4628L0.0284494 10.6193C-0.0262337 10.4826 0.00110783 10.3458 0.0831326 10.1818C0.137816 10.0451 0.247182 9.93572 0.411232 9.88104L3.47349 8.56864C3.58286 8.51396 3.71956 8.51396 3.85627 8.5413C3.99298 8.59598 4.12969 8.65067 4.23905 8.76003L5.57879 10.4005C6.64511 9.90838 7.60207 9.25218 8.42232 8.40459C9.24256 7.58435 9.9261 6.62739 10.4183 5.56107L8.77776 4.22133C8.66839 4.11196 8.58637 4.0026 8.55902 3.86589C8.50434 3.72918 8.53168 3.59247 8.58637 3.45577L9.89876 0.393508C9.95345 0.256801 10.0355 0.147434 10.1995 0.0654094C10.3362 0.0107262 10.4729 -0.0166153 10.637 0.0107262L13.4805 0.666924Z" fill="#454D58"/></svg>
				<a class="font-lexend" href="tel:<?=get_field('phone_number', 'option')?>"><?=get_field('phone_number', 'option')?></a> <span class="d-inline-block dot"></span> <strong>Toll Free: </strong><a class="font-lexend" href="tel:<?=get_field('toll_free', 'option')?>"><?=get_field('toll_free', 'option')?></a>

			</div>
		</div>
	</div>
</section>