<article id="post-<?php the_ID(); ?>" class="blue-gradient page-404 container-fluid inner">
	<div class="entry-header row innerin">
		<div class="col mx-auto"><h1 class="text-white font-lexend font-bold entry-title heading-404">404</h1></div>
	</div>
	<!-- .entry-header -->

	<div class="row innerin">
		<div class="entry-content col mx-auto">
			<p class="text-white ">This page appears to be missing or never existed.</p>
			<p><a href="<?=get_home_url()?>" class="mt-4 btn btn-blue on-dark">Go back home</a></p>
		</div><!-- .entry-content -->
	</div>
</article><!-- #post-<?php the_ID(); ?> -->