<article id="post-<?php the_ID(); ?>" class="container-fluid inner">
	<div class="entry-content row innerin entry-content__article">
		<div class="col-lg-8 col-xl-8 mx-auto py-5 article-container">
			<h1 class="entry-title font-lexend font-40 mb-5 mt-5"><?php the_title(); ?></h1>
			<?php the_content(); ?>

			<?php $include_callout = get_field('include_callout'); ?>
			<?php if($include_callout): ?>
				<div class="article-callout p-5 section--callout">
					<div class="article-callout__wrapper text-center">
						<?php while(have_rows('callout', 'options')):the_row(); ?>
						<h3 class="font-lexend font-30 line-height-36 font-normal text-center"><?=get_sub_field('heading'); ?></h3>

						<a href="<?=get_sub_field('button_link')['url']?>" class="my-3 btn btn-blue on-light d-inline-block mx-auto contact-btn"><?=get_sub_field('button_text'); ?></a>
						<?php endwhile; ?>
					</div>
				</div>
			<?php endif; ?>

			<!-- Related Articles -->
			<h4 class="mt-5 related-article-heading font-lexend font-36 mb-5">Related Articles</h4>
			<?php
			$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 5,'post_type' => 'article', 'post__not_in' => array($post->ID) ) );

			?>
			<div class="related-articles-slider" id="related_articles">
			<?php
			if( $related ) foreach( $related as $post ) {
			setup_postdata($post); ?>
				<div class="article-wrapper related-articles__slide">
				<?php  the_post_thumbnail(); ?>
				<a class="article" href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
					<span class="article__title font-bold font-lexend font-24 text-white"><?php the_title(); ?></span>
					<span class="article__arrow"><svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.5357 0.168161C12.6518 0.0560538 12.8259 0 13.058 0C13.2321 0 13.4063 0.0560538 13.5223 0.168161L25.7679 12.0516C25.8839 12.1637 26 12.3318 26 12.5C26 12.7242 25.8839 12.8363 25.7679 12.9484L13.5223 24.8318C13.4063 24.9439 13.2321 25 13.058 25C12.8259 25 12.6518 24.9439 12.5357 24.8318L12.1295 24.3834C12.0134 24.2713 11.9554 24.1592 11.9554 23.935C11.9554 23.7668 12.0134 23.5987 12.1295 23.4305L22.5179 13.4529H0.696429C0.464286 13.4529 0.290179 13.3969 0.174107 13.2848C0.0580357 13.1726 0 13.0045 0 12.7803V12.2197C0 12.0516 0.0580357 11.8834 0.174107 11.7713C0.290179 11.6592 0.464286 11.5471 0.696429 11.5471H22.5179L12.1295 1.56951C12.0134 1.4574 11.9554 1.28924 11.9554 1.06502C11.9554 0.896861 12.0134 0.7287 12.1295 0.616592L12.5357 0.168161Z" fill="white"></path></svg></span>
				</a>
				</div>
			<?php }
			wp_reset_postdata(); ?>
			</div>

			<div class="slider-component-container">
				<button class="slick-arrow related-prev slick-prev"><svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.1875 0.0950226C7.125 0.0316742 7.03125 0 6.90625 0C6.8125 0 6.71875 0.0316742 6.65625 0.0950226L0.125 6.74661C0.0625 6.80996 0 6.90498 0 7C0 7.1267 0.0625 7.19005 0.125 7.25339L6.65625 13.905C6.71875 13.9683 6.8125 14 6.90625 14C7.03125 14 7.125 13.9683 7.1875 13.905L7.8125 13.2715C7.875 13.2081 7.90625 13.1448 7.90625 13.0181C7.90625 12.9231 7.875 12.8281 7.8125 12.733L2.96875 7.82353H13.625C13.75 7.82353 13.8438 7.79186 13.9062 7.72851C13.9688 7.66516 14 7.57014 14 7.44344V6.55656C14 6.46154 13.9688 6.36652 13.9062 6.30317C13.8438 6.23982 13.75 6.17647 13.625 6.17647H2.96875L7.8125 1.26697C7.875 1.20362 7.90625 1.1086 7.90625 0.981901C7.90625 0.886878 7.875 0.791855 7.8125 0.728507L7.1875 0.0950226Z" fill="#454D58"/></svg></button>
				<div class="dots-container" id="related-article-dots"></div>
				<button class="slick-arrow related-next slick-next"><svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.8125 0.0950226C6.875 0.0316742 6.96875 0 7.09375 0C7.1875 0 7.28125 0.0316742 7.34375 0.0950226L13.875 6.74661C13.9375 6.80996 14 6.90498 14 7C14 7.1267 13.9375 7.19005 13.875 7.25339L7.34375 13.905C7.28125 13.9683 7.1875 14 7.09375 14C6.96875 14 6.875 13.9683 6.8125 13.905L6.1875 13.2715C6.125 13.2081 6.09375 13.1448 6.09375 13.0181C6.09375 12.9231 6.125 12.8281 6.1875 12.733L11.0312 7.82353H0.375C0.25 7.82353 0.15625 7.79186 0.09375 7.72851C0.03125 7.66516 0 7.57014 0 7.44344V6.55656C0 6.46154 0.03125 6.36652 0.09375 6.30317C0.15625 6.23982 0.25 6.17647 0.375 6.17647H11.0312L6.1875 1.26697C6.125 1.20362 6.09375 1.1086 6.09375 0.981901C6.09375 0.886878 6.125 0.791855 6.1875 0.728507L6.8125 0.0950226Z" fill="#454D58"/></svg></button>
			</div>
			<!-- end related articles -->

		</div>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->