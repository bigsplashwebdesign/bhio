<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header"><h1 class="entry-title"><?php the_title(); ?></h1></div><!-- .entry-header -->

	<div class="entry-content container-fluid inner">
		<div class="row innerin">
			<div class="col">
				<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'bigsplash' ), 'after' => '</div>' ) ); ?>
			</div>
		</div>
	</div><!-- .entry-content -->
	
</article><!-- #post-<?php the_ID(); ?> -->