<?php
class BigSplashUser {
	public $ID = null;
	public $first_name = null;
	public $last_name = null;
	public $role = null;
	public $email = null;
	public $logged_in = false;
	public $user_login = null;
	
	public function __construct( $user_id = null ) {
		if ( is_user_logged_in() ) {
			if ( $user_id ) {
				$current_user = get_userdata( $user_id );
				$this->logged_in = false;
			} else {
				global $current_user;
				get_currentuserinfo();
				$this->logged_in = true;
			}
			$this->ID = $current_user->ID;
			$this->user_login = $current_user->user_login;
			$this->first_name = $current_user->first_name;
			$this->last_name = $current_user->last_name;
			$this->role = key( $current_user->caps );
			$this->email = $current_user->data->user_email;
		}
	}
}
class BigSplashRegisterUser {
	public $notifications = array();
	public $ID = null;
	
	public function __construct( $user_name, $email, $first_name, $last_name, $role ) {
		$errors = $this->validate( $user_name, $email, $first_name, $last_name );
		if ( $errors === 0 ) {
			$random_password = wp_generate_password();
			
			$userdata = array(
			    'user_login'  =>  $user_name,
			    'user_pass'   =>  $random_password,
			    'user_email' => $email,
			    'role' => $role,
			    'first_name' => $first_name,
			    'last_name' => $last_name,
			    'display_name' => $user_name
			);
			
			$user_id = wp_insert_user( $userdata ) ;
			if ( $user_id ) {
				update_user_meta( $user_id, 'subscribed', 1);
				update_user_meta( $user_id, 'state', 'ALL');
				update_user_meta( $user_id, 'default_password_nag', 1);
				
				wp_new_user_notification( $user_id, $random_password );
				$this->ID = $user_id;
				$this->notifications[] = "Thank you for signing up.  An email has been sent to you with your password.";
			} else {
				$this->notifications[] = "There was an error registering your account.";
				$content = $email . "\n\r";
				$content .= "<pre>".print_r($user_id,1)."</pre>";
				wp_mail( "chad@bigsplashwebdesign.com", "Registration Error", $content );
			}
		}
		
	}
	
	public function validate( $user_name, $email, $first_name, $last_name ) {
		$errors = 0;
		
		if( empty( $first_name ) ) {
			$this->notifications[] = "Please enter your first name";
			$errors++;
		}
		
		if( empty( $last_name ) ) {
			$this->notifications[] = "Please enter your last name";
			$errors++;
		}
		
		if ( empty( $email ) ) {
			$this->notifications[] = "Please enter your email address";
			$errors++;
		}
		if ( !empty( $email ) ) {
			if ( email_exists( $email ) ) {
				$this->notifications[] = "That email is already registered";
				$errors++;
			}
			if ( !is_email( $email ) ) {
				$this->notifications[] = "Email address is invalid, please enter a proper email address";
				$errors++;
			}
		}
		
		if( empty( $user_name ) ) {
			$this->notifications[] = "Please enter a username";
			$errors++;
		}
		if ( username_exists( $user_name ) ) {
			$this->notifications[] = "That username already exists";
			$errors++;
		}
		
		return $errors;
	}
}
?>