<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package bigsplash
 * @since bigsplash 1.0
 */

get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">

			<?php get_template_part( 'template-parts/post/content', 'none' ); ?>

		</div><!-- #content -->
	</div><!-- #primary .site-content -->

<?php get_footer(); ?>