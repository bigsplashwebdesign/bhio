<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package bigsplash
 * @since bigsplash 1.0
 */

get_header(); ?>
<div id="primary" class="site-content">
	<div id="content" role="main">

		<?php while ( have_posts() ) : the_post(); ?>
			<!-- Section: Hero -->
			<section class="container-fluid inner section--hero px-0 px-lg-3">
				<div class="row innerin align-items-center py-md-5 pt-lg-5 pb-lg-0 py-xl-5">
					<?php while(have_rows('hero_section')) : the_row(); ?>
					<div class="col-xl-6 section--hero__video px-0 px-lg-3">
						<div class="has-shadow embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/549456610?autoplay=1&showinfo=0&modestbranding=1&loop=1&mute=1&background=1"></iframe>
						</div>
					</div>

					<div class="my-5 my-xl-0 col-xl-6 section--hero__text px-lg-5 d-lg-flex align-items-center justify-content-center flex-lg-column">
						<h1 class="mb-4 main-heading font-lexend font-48 text-center font-bold"><?=get_sub_field('heading')?></h1>
						<p class="text-center mx-lg-5"><?=get_sub_field('text')?></p>
					</div>
					<?php endwhile; ?>
				</div>
			</section>


			<!-- Section: Specialties -->
			<section class="container-fluid inner section--specialties bg-light-gray">
				<?php while(have_rows('our_specialties')) : the_row(); ?>
				<div class="row inner">
					<div class="col mb-5"><h2 class="section-heading font-40 font-lexend font-bold text-center"><?=get_sub_field('heading') ?></h2></div>
				</div>
				<div class="row innerin  justify-content-center">
					<?php $services = get_sub_field('specialties_links'); ?>
					<?php foreach($services as $index=>$service): ?>
					<div class="col-md-6 col-lg-4 service-link">
						<a class="font-20 font-bold text-center text-white justify-content-center d-flex align-items-center bg-main-blue font-lexend" href="<?=$service['link']['url']?>"><span><?=$service['link']['title']?></span></a>
					</div>
					<?php endforeach; ?>
				</div>
				<?php endwhile; ?>

				<div class="row innerin">
					<div class="col-lg-6 mx-auto text-center pt-5 mt-4 direct-shop">
						<p class="mb-4 font-lexend font-bold font-30">Save time and shop insurance directly!</p>
						<p class="font-20">Some of our products are available through self-service.<br><a href="https://insuremenowdirect.com/agent/maryladmirault-sullivan/" target="_blank">Click here to instantly get a quote, apply, and get covered</a></p>
					</div>
				</div>
			</section>
			
			<!-- Section: Why Nassau Bay Agency -->
			<section class="container-fluid inner section--why bg-diff-blue">
				<div class="row innerin">
					<div class="col-sm-10 col-lg-9 mx-auto col-xl-5 mb-5 mb-lg-0">
						<div class="why-video">
							<a href="#" class="has-shadow-alternate why-video__link video-btn" id="why-video-link" data-video-src="https://player.vimeo.com/video/549451027" data-toggle="modal" data-target="#video-modal">
								<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="#fff" class="bi bi-play-circle-fill" viewBox="0 0 16 16"><path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5z"/></svg>
								<img src="<?=get_template_directory_uri() . '/assets/images/home/Video_image.png'?>" alt="" class="img-fluid w-100">
							</a>
						</div>
					</div>
					<div class="col-sm-10 col-lg-9 mx-auto col-xl-6 offset-xl-1 mt-lg-5 mt-xl-0">
						<h3 class="font-lexend font-bold font-40 mb-4 text-white">Why Nassau Bay Agency?</h3>
						<p class="text-white">Where you choose to purchase your insurance protection makes a difference. Two key reasons to choose us over others are:</p>
						<div class="check my-5 d-md-flex">
							<div class="mb-5 mb-lg-0 check__content left text-center">
								<svg width="35" height="35" viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="17.5" cy="17.5" r="17.5" fill="#33A6DE"/><path d="M25.1912 10.6555C25.2766 10.5701 25.3621 10.5273 25.533 10.5273C25.6611 10.5273 25.7893 10.5701 25.9175 10.6555L27.1138 11.8945C27.1992 11.98 27.2847 12.1082 27.2847 12.2363C27.2847 12.4072 27.1992 12.5354 27.1138 12.6208L14.2964 25.4382C14.2109 25.5237 14.0828 25.5664 13.9546 25.5664C13.7837 25.5664 13.6555 25.5237 13.5701 25.4382L7.8877 19.7131C7.75952 19.6277 7.7168 19.5422 7.7168 19.3713C7.7168 19.2432 7.75952 19.115 7.8877 18.9868L9.08398 17.7905C9.16943 17.7051 9.29761 17.6196 9.46851 17.6196C9.59668 17.6196 9.72485 17.7051 9.8103 17.7905L13.9546 21.9348L25.1912 10.6555Z" fill="white"/></svg>
								<span class="color-light-blue text-center">Our commitment to customer service</span>
							</div>
							<div class="check__content right text-center">
								<svg width="35" height="35" viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="17.5" cy="17.5" r="17.5" fill="#33A6DE"/><path d="M25.1912 10.6555C25.2766 10.5701 25.3621 10.5273 25.533 10.5273C25.6611 10.5273 25.7893 10.5701 25.9175 10.6555L27.1138 11.8945C27.1992 11.98 27.2847 12.1082 27.2847 12.2363C27.2847 12.4072 27.1992 12.5354 27.1138 12.6208L14.2964 25.4382C14.2109 25.5237 14.0828 25.5664 13.9546 25.5664C13.7837 25.5664 13.6555 25.5237 13.5701 25.4382L7.8877 19.7131C7.75952 19.6277 7.7168 19.5422 7.7168 19.3713C7.7168 19.2432 7.75952 19.115 7.8877 18.9868L9.08398 17.7905C9.16943 17.7051 9.29761 17.6196 9.46851 17.6196C9.59668 17.6196 9.72485 17.7051 9.8103 17.7905L13.9546 21.9348L25.1912 10.6555Z" fill="white"/></svg>
								<span class="color-light-blue text-center">Our approach of helping you fit insurance coverage to your needs and not just buy a policy.</span>
							</div>
						</div>
						<p class="text-white">We’re proud to serve families and businesses of all sizes for multiple generations. We look forward to the possibility of serving you.</p>
						<p class="mt-5"><a href="<?=get_permalink(6)?>" class="btn btn-blue on-dark text-white">Learn more about us</a></p>
					</div>
				</div>
			</section>

			<!-- Section: Help Desk -->
			<section class="container-fluid inner section--helpdesk">
				<div class="row inner">
					<div class="col-lg-6 mx-auto text-center">
						<h4 class="mb-3 font-lexend font-bold font-40">Help Desk</h4>
						<p>Check out our Help Desk articles for useful tips, resources, and news relating to all your insurance needs. Still have questions? Feel free to give us a call instead!</p>
					</div>
				</div>

				<div class="row innerin align-items-lg-stretch pt-4">
					<?php $args = array(
						'post_type' => 'article',
						'posts_per_page' => 3
					); ?>
					<?php $articles = new WP_Query($args); 
					if($articles->have_posts()):
						while($articles->have_posts()) : $articles->the_post();
					?>
					<div class="col-lg-4 article-wrapper d-lg-flex align-items-end">
						<a href="<?=get_the_permalink()?>" class="article w-100">
							<span class="article__title font-bold font-lexend font-24 text-white"><?=get_the_title()?></span>
							<span class="article__arrow">
								<svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.5357 0.168161C12.6518 0.0560538 12.8259 0 13.058 0C13.2321 0 13.4063 0.0560538 13.5223 0.168161L25.7679 12.0516C25.8839 12.1637 26 12.3318 26 12.5C26 12.7242 25.8839 12.8363 25.7679 12.9484L13.5223 24.8318C13.4063 24.9439 13.2321 25 13.058 25C12.8259 25 12.6518 24.9439 12.5357 24.8318L12.1295 24.3834C12.0134 24.2713 11.9554 24.1592 11.9554 23.935C11.9554 23.7668 12.0134 23.5987 12.1295 23.4305L22.5179 13.4529H0.696429C0.464286 13.4529 0.290179 13.3969 0.174107 13.2848C0.0580357 13.1726 0 13.0045 0 12.7803V12.2197C0 12.0516 0.0580357 11.8834 0.174107 11.7713C0.290179 11.6592 0.464286 11.5471 0.696429 11.5471H22.5179L12.1295 1.56951C12.0134 1.4574 11.9554 1.28924 11.9554 1.06502C11.9554 0.896861 12.0134 0.7287 12.1295 0.616592L12.5357 0.168161Z" fill="white"/></svg>
							</span>
						</a>
					</div>
					<?php endwhile; endif; ?>
				</div>
				<div class="row innerin">
					<div class="col-lg-4 text-center my-5 mx-auto">
						<a href="<?=get_home_url() . '/help-desk'?>" class="color-main-blue see-all font-lexend font-bold">See all articles</a>
					</div>
				</div>
			</section>

			<?php get_template_part('template-parts/components/content', 'bottom-callout'); ?>
		<?php endwhile; // end of the loop. ?>

	</div><!-- #content -->
</div><!-- #primary .site-content -->

<!-- Video Modal -->
<div class="video-modal modal fade" id="video-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<button type="button" class="close modal-dismiss" data-dismiss="modal" aria-label="Close"><span></span></button>

	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">

			<div class="modal-body p-0">
				<!-- 16:9 aspect ratio -->
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="" id="video"
						allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>

		</div>
	</div>
</div>
<!-- End video modal -->
<?php get_footer(); ?>