<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package bigsplash
 * @since bigsplash 1.0
 */
?>

	</div><!-- #main -->

	<footer id="colophon" class="site-footer container-fluid bg-darker-blue pt-4">
		<div class="site-footer__upper row innerin py-5">
			<div class="col-lg-4 col-xl-3 mb-5">
				<a href="<?=get_home_url()?>" class="footer-logo">
					<img src="<?=get_template_directory_uri() . '/assets/images/global/logo-white.svg'?>" alt="Footer logo" class="img-fluid" />
				</a>

				<div class="footer-phone mt-4">
					<svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M13.4805 0.666924C13.6446 0.694266 13.7539 0.776291 13.8633 0.885657C13.9453 1.02236 14 1.15907 14 1.29578C14 3.59247 13.4258 5.72512 12.2775 7.69371C11.1291 9.60762 9.62535 11.1388 7.71144 12.2598C5.74284 13.4081 3.6102 13.9823 1.3135 13.9823C1.14945 13.9823 1.01275 13.9549 0.90338 13.8456C0.766672 13.7635 0.684647 13.6268 0.684647 13.4628L0.0284494 10.6193C-0.0262337 10.4826 0.00110783 10.3458 0.0831326 10.1818C0.137816 10.0451 0.247182 9.93572 0.411232 9.88104L3.47349 8.56864C3.58286 8.51396 3.71956 8.51396 3.85627 8.5413C3.99298 8.59598 4.12969 8.65067 4.23905 8.76003L5.57879 10.4005C6.64511 9.90838 7.60207 9.25218 8.42232 8.40459C9.24256 7.58435 9.9261 6.62739 10.4183 5.56107L8.77776 4.22133C8.66839 4.11196 8.58637 4.0026 8.55902 3.86589C8.50434 3.72918 8.53168 3.59247 8.58637 3.45577L9.89876 0.393508C9.95345 0.256801 10.0355 0.147434 10.1995 0.0654094C10.3362 0.0107262 10.4729 -0.0166153 10.637 0.0107262L13.4805 0.666924Z" fill="white"/></svg>
					<a class="font-lexend text-white footer-link" href="tel:<?=get_field('phone_number', 'option')?>" class=""><?=get_field('phone_number', 'option')?></a>
				</div>
				<div class="footer-address mt-4">
					<svg width="11" height="14" viewBox="0 0 11 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M4.70312 13.7266C4.8125 13.918 5.00391 14 5.25 14C5.46875 14 5.66016 13.918 5.79688 13.7266L7.62891 11.1016C8.53125 9.78906 9.13281 8.91406 9.43359 8.44922C9.84375 7.79297 10.1172 7.24609 10.2812 6.80859C10.418 6.37109 10.5 5.85156 10.5 5.25C10.5 4.32031 10.2539 3.44531 9.78906 2.625C9.29688 1.83203 8.66797 1.20312 7.875 0.710938C7.05469 0.246094 6.17969 0 5.25 0C4.29297 0 3.41797 0.246094 2.625 0.710938C1.80469 1.20312 1.17578 1.83203 0.710938 2.625C0.21875 3.44531 0 4.32031 0 5.25C0 5.85156 0.0546875 6.37109 0.21875 6.80859C0.355469 7.24609 0.628906 7.79297 1.06641 8.44922C1.33984 8.91406 1.94141 9.78906 2.87109 11.1016C3.60938 12.168 4.21094 13.043 4.70312 13.7266ZM5.25 7.4375C4.64844 7.4375 4.12891 7.24609 3.69141 6.80859C3.25391 6.37109 3.0625 5.85156 3.0625 5.25C3.0625 4.64844 3.25391 4.15625 3.69141 3.71875C4.12891 3.28125 4.64844 3.0625 5.25 3.0625C5.85156 3.0625 6.34375 3.28125 6.78125 3.71875C7.21875 4.15625 7.4375 4.64844 7.4375 5.25C7.4375 5.85156 7.21875 6.37109 6.78125 6.80859C6.34375 7.24609 5.85156 7.4375 5.25 7.4375Z" fill="white"/></svg>

					<span class="font-lexend text-white"><?=get_field('address', 'option')?></span>
				</div>
				<div class="footer-book-btn mt-4 d-flex align-items-center">
					<?php while(have_rows('appointment_button','option')) : the_row(); ?>
					<a href="<?=get_sub_field('button_link')?>" class="d-inline-block book-link btn btn-blue on-dark font-lexend"><?=get_sub_field('button_text')?></a>
					<?php endwhile; ?>

					<?php while(have_rows('social_links', 'option')) : the_row(); ?>
					<a target="_blank" href="<?=get_sub_field('service_link');?>" class="mb-0 ml-3 facebook_icon">
						<img src="<?=get_template_directory_uri() . '/assets/images/global/fb_icon.svg'?>" alt="Facebook" class="fb_icon">
					</a>
					<?php endwhile; ?>

				</div>
			</div>
			<div class="col-lg-5 col-xl-7 px-lg-5">
				<h3 class="mb-4 footer-heading font-lexend font-bold font-20">
					<a class="text-white" href="<?=get_permalink(4)?>">Specialties</a>
				</h3>

				<?php wp_nav_menu( array(
					'theme_location' => 'footer_specialties',
					'menu_class'		 => 'footer-menu font-lexend pl-0 specialties-pages',
					'menu_id' => 'footer-navigation-menu'
				) ); ?>

				<?php
				//$menu_items = wp_get_nav_menu_items(4);
				//$count = count($menu_items);
				//$row = 0;
				?>
				<!-- <div class="specialties-menu __left">
				<ul class="footer-menu pl-0 specialties-pages">
					<?php foreach($menu_items as $menu_item) {?>
						<li class="text-white footer-menu__item"><a href="<?=$menu_item->url; ?>" class="d-inline-block text-white"><?=$menu_item->title; ?></a></li>
					<?php } ?>
				</ul>
				</div> -->

			</div>
			<div class="col-lg-3 col-xl-2 pl-lg-4">
				<h3 class="mt-4 mt-lg-0 mb-4 text-white footer-heading font-lexend font-bold font-20">
					Navigate
				</h3>

				<?php wp_nav_menu( array(
					'theme_location' => 'footer_navigate',
					'menu_class'		 => 'footer-menu font-lexend pl-0 footer-navigation__nav',
					'menu_id' => 'footer-navigation-menu'
				) ); ?>
			</div>
		</div>
		<div class="site-info row innerin py-5">
			<div class="copy col-12 text-center font-lexend text-white">
				<p class="mb-0">&copy; <?php bloginfo( 'name' ); ?>
				| <a href="<?=get_permalink(95);?>" class="text-white footer-link__lower">Legal Terms</a>
				| <a href="<?=get_privacy_policy_url();?>" class="text-white footer-link__lower">Privacy Policy</a>
				| <a class="text-white" target="_blank" href="https://www.bigsplashwebdesign.com/web-design/">Custom Web Design</a> by <a target="_blank" class="text-white" href="https://www.bigsplashwebdesign.com/">Big Splash Web Design & Marketing</a></p>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon .site-footer -->
</div><!-- #page .hfeed .site -->

<?php wp_footer(); ?>
</body>
</html>
