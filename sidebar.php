<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package bigsplash
 * @since bigsplash 1.0
 */
?>
<div id="secondary" role="complementary">
	<?php do_action( 'before_sidebar' ); ?>
</div><!-- #secondary .widget-area -->
