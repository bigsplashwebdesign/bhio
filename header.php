<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Lexend:wght@100;300;400;500;600;700;800&family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;1,300;1,400;1,600;1,700&display=swap" rel="stylesheet">
<?php wp_head(); ?>
</head>

<?php
	$browser = new Wolfcast\BrowserDetection();
	if($browser->getPlatform() == Wolfcast\BrowserDetection::PLATFORM_IOS):
		$ios = 'ios';
	else:
		$ios = '';
	endif;
?>

<body <?php body_class($ios); ?>>
<div id="page" class="site px-0">
	<?php do_action( 'before' ); ?>
	<header id="masthead" class="site-header inner">
		<div id="logo" class="innerin logo__desktop d-none d-lg-block logo-container py-4 text-center"><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?=get_template_directory_uri() . '/assets/images/global/bhio_logo.svg' ?>" alt="BHIO Logo"></a>

		<!-- Google Translate -->
		<div class='google-translate-dropdown desktop d-none d-lg-block'><div id="google_translate_element"></div></div>
		<script type="text/javascript">
		function googleTranslateElementInit() {
			new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'es', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
		}
		jQuery( function( $ ) {
			function moveTranslateDropdown() {
			var dropdownGoogle = $('#google_translate_element');
			var winWidth = $(window).width();
			var parent;
			googleTranslateElementInit();
			if(winWidth < 992) {
				parent = $('.google-translate-dropdown.mobile');
			}
			else parent = $('google-translate-dropdown.desktop');
			parent.html(dropdownGoogle.outerHTML);
		}
		moveTranslateDropdown();
		$(window).resize(moveTranslateDropdown);	
		});
		</script>
		
		<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
		<!-- end google translate -->
		</div>

		<div class="mobile-menu-wrapper py-2 d-flex align-items-center justify-content-between">
			<button class="d-lg-none" id="mobile-menu">
				<span class="bar __top"></span>
				<span class="bar __middle"></span>
				<span class="bar __bottom"></span>
			</button>

			<div class="phone-icon mr-4 d-lg-none">
				<a href="tel:83838388" class="phone-icon__icon">
					<svg width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M19.2579 0.994757C19.4922 1.03554 19.6485 1.15788 19.8047 1.32101C19.9219 1.52492 20 1.72883 20 1.93273C20 5.35839 19.1798 8.53935 17.5393 11.4756C15.8988 14.3303 13.7505 16.6141 11.0163 18.2862C8.20406 19.999 5.15742 20.8554 1.87643 20.8554C1.64208 20.8554 1.44678 20.8146 1.29054 20.6515C1.09525 20.5291 0.978068 20.3252 0.978068 20.0805L0.040642 15.8393C-0.0374768 15.6354 0.00158262 15.4314 0.118761 15.1868C0.19688 14.9828 0.353117 14.8197 0.587474 14.7382L4.96213 12.7806C5.11836 12.6991 5.31366 12.6991 5.50896 12.7399C5.70426 12.8214 5.89955 12.903 6.05579 13.0661L7.9697 15.513C9.49302 14.7789 10.8601 13.8002 12.0319 12.536C13.2037 11.3125 14.1801 9.88515 14.8832 8.29466L12.5397 6.29637C12.3834 6.13324 12.2662 5.97011 12.2272 5.76621C12.1491 5.5623 12.1881 5.35839 12.2662 5.15448L14.1411 0.586941C14.2192 0.383033 14.3364 0.219907 14.5707 0.097562C14.766 0.0159988 14.9613 -0.0247828 15.1957 0.0159988L19.2579 0.994757Z" fill="white"/></svg>
				</a>
			</div>
		</div>

		<div class="logo-mobile d-lg-none bg-white">
			<a href="<?=get_home_url()?>" class="text-center mobile-logo d-block">
				<img src="<?=get_template_directory_uri() . '/assets/images/global/mobile-logo.svg'?>" alt="BHIO Mobile Logo" class="mobile-logo-img img-fluid mx-auto d-inline-block w-100">
			</a>
		</div>

		<nav id="nav" class="bg-blue-gradient px-lg-3 site-navigation main-navigation d-lg-flex align-items-center justify-content-between">

			<div class="main-navigation__container col d-lg-flex justify-content-between align-items-center w-100 innerin px-0">
				<?php wp_nav_menu( array(
					'theme_location' => 'primary',
					'menu_class'		 => 'main-navigation__nav',
					'walker' => new Walker_Nav_Menu_Wrapper
				) ); ?>

				<div class="contact-nav font-lexend">
					<ul class="contact-nav-menu">
						<?php if(get_field('phone_number', 'option')): ?>
						<li class="d-none d-lg-inline-block contact-nav-menu__item phone">
							<svg class="d-inline-block mr-1" width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.51949 0.685967C0.355441 0.713309 0.246075 0.795334 0.136708 0.9047C0.0546837 1.04141 0 1.17812 0 1.31482C0 3.61152 0.574173 5.74416 1.72252 7.71275C2.87087 9.62667 4.37465 11.1578 6.28856 12.2788C8.25716 13.4271 10.3898 14.0013 12.6865 14.0013C12.8505 14.0013 12.9873 13.974 13.0966 13.8646C13.2333 13.7826 13.3154 13.6459 13.3154 13.4818L13.9716 10.6383C14.0262 10.5016 13.9989 10.3649 13.9169 10.2008C13.8622 10.0641 13.7528 9.95476 13.5888 9.90008L10.5265 8.58769C10.4171 8.533 10.2804 8.533 10.1437 8.56034C10.007 8.61503 9.87031 8.66971 9.76095 8.77908L8.42121 10.4196C7.35489 9.92742 6.39793 9.27122 5.57768 8.42364C4.75744 7.60339 4.0739 6.64643 3.58175 5.58011L5.22224 4.24037C5.33161 4.13101 5.41363 4.02164 5.44098 3.88493C5.49566 3.74822 5.46832 3.61152 5.41363 3.47481L4.10124 0.412551C4.04655 0.275843 3.96453 0.166477 3.80048 0.0844524C3.66377 0.0297692 3.52706 0.00242762 3.36302 0.0297692L0.51949 0.685967Z" fill="white"/>
							</svg>
							<a href="tel:<?=get_field('phone_number', 'option')?>" class="phone-link"><?=get_field('phone_number', 'option')?></a>
						</li>
						<?php endif; ?>
						<?php while(have_rows('appointment_button','option')) : the_row(); ?>
						<li class="contact-nav-menu__item book">
							<a target="_blank" href="<?=get_sub_field('button_link');?>" class="book-link"><?=get_sub_field('button_text')?></a>
						</li>
						<?php endwhile; ?>
					</ul>

					<div class='google-translate-dropdown ml-3 mobile d-lg-none'></div>
				</div>
			</div>

		</nav><!-- .site-navigation .main-navigation -->
	</header><!-- #masthead .site-header -->

	<div id="main">
